// Setup code
// ------------------------------------------------------------------------------
var express = require('express');
var app = express();
app.set('port', process.env.PORT || 3000);

var handlebars = require('express-handlebars');
app.engine('handlebars', handlebars({ defaultLayout: 'main' }));
app.set('view engine', 'handlebars');

var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }));

// required the filesystem module @martin
var fs = require('fs');

// require formidable for handling audio/video uploads
var formidable = require("formidable");

// use express-session to create in-memory sessions @martin
var session = require('express-session');
app.use(session({
    resave: false,
    saveUninitialized: false,
    secret: "teamJAHM"
}));

// require the JavaScript to process image uploads from Froala @jingyuan
var upload_image = require("./image_upload.js");

// allow us to use SQLite3 from node.js & connect to a database @martin
var sqlite3 = require('sqlite3').verbose();
//returns a new Database object, and opens the SQLite database 'blod-db.db‘ automatically
var db = new sqlite3.Database('blog-db.db');

//import external DAO module which contains database statements
var dao = require('./dao.js');

// Code for authentication starts from here @martin
// ---------------------------------------------------------------------------------

// require the 'passport' module for authentication
var passport = require('passport');
// use the local authentication strategy
var LocalStrategy = require('passport-local').Strategy;

// define the local authentication strategy
var localStrategy = new LocalStrategy(function (username, password, done) {

    // // query the blog database for the supplied username and retrieve all columns
    // db.all("SELECT * FROM Users WHERE username = ?", [username], function (err, rows) {

    //     // if the query returns no rows, then the username was not found in the database
    //     if (rows.length == 0) {
    //         return done(null, false, { message: 'Invalid username!' });
    //     }

    //     // there should only be 1 row returned by SQL query if the username was found
    //     var user = rows[0];

    //     // retrieve the password string from the 'rows' object returned by the query
    //     var userPasswordInDb = rows[0].password;

    //     
    //     if (userPasswordInDb !== password) {
    //         return done(null, false, { message: 'Invalid password!' });
    //     };

    //     // if the above validation has passed, then user is authenticated
    //     done(null, user);
    // });   

    // query the blog database for the supplied username and retrieve all columns
    dao.getUser(username, function (user) {
        // there should only be 1 row returned by SQL query if the username was found
        if (!user) {
            return done(null, false, { message: 'Invalid user' });
        };

        // if the provided password, does not match what is in the database
        if (user.password !== password) {
            return done(null, false, { message: 'Invalid password' });
        };

        // if the above validation has passed, then user is authenticated
        done(null, user);
    });

});

// method to be called to save the currently logged in username to the session
passport.serializeUser(function (user, done) {
    done(null, user.username);
});

// method to be called to retrieve all data in the database related to the provided username
passport.deserializeUser(function (username, done) {

    // // query the blog database for the supplied username
    // db.all("SELECT * FROM Users WHERE username = ?", [username], function (err, rows) {  
    //     if (rows.length > 0) {
    //         user = rows[0];
    //         done(null, user);
    //     }
    // });

    // query the blog database for the supplied username
    dao.getUser(username, function (user) {
        done(null, user);
    });

});

// passport should use the 'local strategy' for authentication
passport.use('local', localStrategy);

// initialise passport
app.use(passport.initialize());
// request passport to use sessions to store its data
app.use(passport.session());

// ---------------------------------------------------------------------------------
// code for authentication ends @martin


// check if user is logged in
function isLoggedIn(req, res, next) {
    // if user is authenticated, execute the next function 
    if (req.isAuthenticated()) {
        return next();
    }

    // redirect them to the login page
    res.redirect("/login");
}

//--------------------- ROUTE HANDLERS -------------------------------------------

//-----Routes for displaying and editing ARTICLES-------------------------------
app.get(['/', '/home', '/article'], function (req, res) {

    dao.getAllArticles(function (articles) {
        var username = null;
        if (req.isAuthenticated()) {
            username = req.user.username;
        }

        var data = {
            username: username,
            articles: articles,
            loggedOut: req.query.loggedOut,
            newArticleCreated: req.query.newArticleCreated,
            accountUpdated: req.query.accountUpdated,
            articleDeleted: req.query.articleDeleted,

            allArticles: true
        }

        res.render('home', data);
    });

});

app.get('/addPost', isLoggedIn, function (req, res) {

    // var data = {
    //     layout: 'no-nav'
    // }

    res.render('addPost');
});

app.post('/addPost', function (req, res) {

    // create a new formidable form object
    var form = new formidable.IncomingForm();

    // when file upload detected, upload file to the multimedia folder
    form.on("fileBegin", function (name, file) {
        // console.log(req.user.username);
        // console.log(articleId);
        file.path =__dirname + "/public/uploads/multimedia/" + req.user.username + "-" + file.name;
    });

    // parse the submitted form data using formidable
    form.parse(req, function (err, fields, files) {

        // check the file type of the upload file
        var fileType = files.fileUpload.type;
        var audioFile = null;
        var videoFile = null;

        // if file type starts with '/audio'
        if (fileType.startsWith('audio')) {
            audioFile = req.user.username + "-" + files.fileUpload.name;
        // else if file type starts with '/video'
        } else if (fileType.startsWith('video')) {
            videoFile = req.user.username + "-" + files.fileUpload.name;
        }

        // LINK to be removed when testing complete!
        var articleDetails = {
            username: req.user.username,
            title: fields.title,
            content: fields.content,
            audio: audioFile,
            video: videoFile,
            link: 'no_link'
        }

        dao.createArticle(articleDetails, function () {
            res.redirect("/home?newArticleCreated=true");
        })
    });

});

app.get('/article/:id', function (req, res) {

    var isArticleAuthor = false;

    dao.getArticle(req.params.id, function (article, comments) {
        var username = null;
        if (req.isAuthenticated()) {
            username = req.user.username;
        }
        
        if (article.author == username) {
            isArticleAuthor = true;
        }

        // add isCommentAuthor flag which enables display of edit and delete button on own comments
        for (var i = 0; i < comments.length; i++) {
            if (comments[i].author == username) {
                comments[i].isCommentAuthor = true;
            }
        }

        var data = {
            username: username,
            articles: article,
            allArticles: false,
            isArticleAuthor: isArticleAuthor,
            comments: comments,
            commentDeleted: req.query.commentDeleted
        }

        res.render('articleView', data);
    });

});

app.post('/editPost', function (req, res) {

    var articleId = req.body.articleId;

    dao.getArticle(articleId, function (article) {
        var username = null;
        if (req.isAuthenticated()) {
            username = req.user.username;
        }

        var data = {
            layout: 'no-nav',
            username: username,
            articles: article,
            allArticles: false,
        }

        res.render('editPost', data);
    });
})

app.post('/saveEditedPost', function (req, res) {

    var articleId = req.body.articleId;
    var newTitle = req.body.title;
    var newContent = req.body.content;

    dao.updateArticle(articleId, newTitle, newContent, function () {
        res.redirect("/article/" + articleId);
    })

});

app.post('/deletePost', function (req, res) {

    var articleId = req.body.articleId;
    
    dao.deleteArticle(articleId, function () {
        res.redirect("/home?articleDeleted=true");
    })

})

//------------------------------------------------------------------------------

//-----Routes for displaying and editing COMMENTS-------------------------------

app.post('/addComment', function (req, res) {

    var username = req.body.username;
    var articleId = req.body.articleId;
    var newComment = req.body.newComment;

    dao.addComment(username, articleId, newComment, function () {
        res.redirect("/article/" + articleId);
    });
});

app.post('/editComment', function (req, res) {

    var commentId = req.body.commentId;

    dao.getComment(commentId, function (comment) {
        var username = null;
        if (req.isAuthenticated()) {
            username = req.user.username;
        }

        var data = {
            layout: 'no-nav',
            username: username,
            comment: comment
        }

        res.render('editComment', data);
    });
})

app.post('/saveEditedComment', function (req, res) {

    var commentId = req.body.commentId;
    var articleId = req.body.articleId;
    var newComment = req.body.content;

    dao.updateComment(commentId, newComment, function () {
        res.redirect("/article/" + articleId);

    })

});

app.post('/deleteComment', function (req, res) {

    var commentId = req.body.commentId;
    var articleId = req.body.articleId;

    dao.deleteComment(commentId, function () {
        res.redirect("/article/" + articleId + "?commentDeleted=true");
    });

});

//------------------------------------------------------------------------------

//-----Routes for USER Activities-----------------------------------------------

app.get('/signup', function (req, res) {

    // generate an array of filenames in the default-avatars folder
    var defaultAvatars = fs.readdirSync(__dirname + '/public/default-avatars');

    var data = {
        layout: 'no-nav',
        passwordFail: req.query.passwordFail,
        userData: req.session.partialUserData,
        defaultAvatars: defaultAvatars
    }
    res.render('signup', data);
});

app.post('/signup', function (req, res) {
    if (req.body.password != req.body.passwordCheck) {
        req.session.partialUserData = {
            fname: req.body.fname,
            lname: req.body.lname,
            dob: req.body.dob,
            country: req.body.country,
            username: req.body.username
        }
        res.redirect('/signup?passwordFail=true');
    }
    else {

        dao.createUser(req.body, function () {
            delete req.session.partialUserData;
            res.redirect('/login?newAccountCreated=true');
        });
    }
});

app.get('/login', function (req, res) {
    if (req.isAuthenticated()) {
        res.redirect("/home");
    }
    else {
        var data = {
            layout: 'no-nav',
            loginFail: req.query.loginFail,
            newAccountCreated: req.query.newAccountCreated
        }
        res.render('login', data);
    }
});


app.post('/login', passport.authenticate('local',
    {
        successRedirect: '/home',
        failureRedirect: '/login?loginFail=true'
    }
));

app.get('/logout', function (req, res) {
    req.logout();
    res.redirect("/home?loggedOut=true");
});

app.get('/account', isLoggedIn, function (req, res) {

    var defaultAvatars = fs.readdirSync(__dirname + '/public/default-avatars');
    if (req.session.partialUserData) {
        var data = {
            layout: 'no-nav',
            passwordFail: req.query.passwordFail,
            defaultAvatars: defaultAvatars,
            userData: req.session.partialEditUserData
        }
        res.render('account', data);
    }
    else {
        dao.getUser(req.user.username, function (user) {
            var data = {
                layout: 'no-nav',
                passwordFail: req.query.passwordFail,
                defaultAvatars: defaultAvatars,
                userData: user
            }
            res.render('account', data);
        });

    }
});

app.post('/accountUpdate', function (req, res) {

    var defaultAvatars = fs.readdirSync(__dirname + '/public/default-avatars');
    if (req.body.password != req.body.passwordCheck) {
        req.session.partialEditUserData = {
            fname: req.body.fname,
            lname: req.body.lname,
            dob: req.body.dob,
            country: req.body.country,
            username: req.body.username
        }
        res.redirect('/account?passwordFail=true');
    }
    else {

        dao.updateUser(req.body, function () {
            delete req.session.partialEditUserData;
            res.redirect('/home?accountUpdated=true');
        });
    }
});

//-----Routes for handling Froala file uploads---------------------------------

// route handler for saving images from Froala editor:
app.post("/image_upload", function (req, res) {

    upload_image(req, function(err, data) {
        
        if (err) {
        return res.status(404).end(JSON.stringify(err));
        }
        res.send(data);
    });
});

//------------------------------------------------------------------------------


// Serve files form "/public" folder
app.use(express.static(__dirname + "/public"));
app.use(express.static(__dirname + "/"));

// --------------------------------------------------------------------------

// Start the server running.
app.listen(app.get('port'), function () {
    console.log('Express started on http://localhost:' + app.get('port'));
});